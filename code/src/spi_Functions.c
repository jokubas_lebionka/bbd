#include "../inc/main.h"
#include <stm32f10x_spi.h>

void spi2_master_init(void){
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
    // RCC_PCLK2Config(RCC_HCLK_Div2);
    NVIC_InitTypeDef NVIC_InitStructure;
    GPIO_InitTypeDef SPI2_Master_GPIO_InitStructure;
    SPI_InitTypeDef  SPI2_Master_InitStructure;

    NVIC_InitStructure.NVIC_IRQChannel = SPI2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* NSS */
    SPI2_Master_GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
    SPI2_Master_GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    SPI2_Master_GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOB, &SPI2_Master_GPIO_InitStructure);  
    /* SCK | MOSI */
    SPI2_Master_GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_15;
    SPI2_Master_GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    SPI2_Master_GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB, &SPI2_Master_GPIO_InitStructure);
    /* MISO */
    SPI2_Master_GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
    SPI2_Master_GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; // AF OD?
    GPIO_Init(GPIOB, &SPI2_Master_GPIO_InitStructure);
    
    SPI_I2S_DeInit(SPI2);
    SPI2_Master_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI2_Master_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI2_Master_InitStructure.SPI_DataSize = SPI_DataSize_16b;
    SPI2_Master_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI2_Master_InitStructure.SPI_CPHA = SPI_CPHA_2Edge; // 1 EDGE ?
    SPI2_Master_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI2_Master_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16; // 2?
    SPI2_Master_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    // SPI2_Master_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(SPI2, &SPI2_Master_InitStructure);
    // SPI_CalculateCRC(SPI2, ENABLE);
    SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_RXNE, ENABLE);
    SPI_SSOutputCmd(SPI2, ENABLE);
    SPI_Cmd(SPI2, ENABLE);
}