#include "../inc/main.h"
#include <stm32f10x_usart.h>

#define BAUDRATE_USART1 115200
#define TX_PIN_USART1 GPIO_Pin_9
#define RX_PIN_USART1 GPIO_Pin_10

void usart1_init_function(void){
    NVIC_InitTypeDef NVIC_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef USART1_GPIO_InitStructure;
    
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	USART1_GPIO_InitStructure.GPIO_Pin = TX_PIN_USART1;
	USART1_GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	USART1_GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOA, &USART1_GPIO_InitStructure);
	USART1_GPIO_InitStructure.GPIO_Pin = RX_PIN_USART1;
	USART1_GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &USART1_GPIO_InitStructure);
	USART_InitStructure.USART_BaudRate = BAUDRATE_USART1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
	USART_Cmd(USART1, ENABLE);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
}

void UART_SendChar(USART_TypeDef* USARTx, char ch) {
	USARTx->DR = ch;
	while (!(USARTx->SR & USART_SR_TC));
}

void UART_SendStr(USART_TypeDef* USARTx, char *str) {
	while (*str) UART_SendChar(USARTx,*str++);
}

