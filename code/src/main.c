#include "../inc/main.h"
#include "../inc/uart_Functions.h"
#include "../inc/misc_Functions.h"
#include "../inc/spi_Functions.h"
#include <string.h>

void USART1_IRQHandler(void){
	if(USART1->SR & USART_SR_RXNE){
        SPI2->DR = USART1->DR;
	    USART1->SR &= ~(USART_SR_RXNE);
	}
}

void SPI2_IRQHandler(void){
    if (SPI2->SR & SPI_SR_RXNE){
        USART1->DR = SPI2->DR;
        SPI2->SR &= ~(SPI_SR_RXNE);
    }
}

int main(void) {
    led1_init(); 
	usart1_init_function();
    spi2_master_init();
    while(1) {
    }
}
