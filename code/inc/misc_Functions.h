#ifndef __MISC_FUNCTIONS_H
#define __MISC_FUNCTIONS_H

#include <stm32f10x.h>
#include <misc.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_conf.h>

void delay(int millis);
void led1_init(void);

#endif
