#ifndef __UART_FUNCTIONS_H
#define __UART_FUNCTIONS_H

#include <stm32f10x.h>
#include <misc.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_conf.h>
#include <stm32f10x_usart.h>


void usart1_init_function(void);
void UART_SendChar(USART_TypeDef* USARTx, char ch);
void UART_SendStr(USART_TypeDef* USARTx, char *str);

#endif
