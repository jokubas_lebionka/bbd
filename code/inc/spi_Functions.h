#ifndef __SPI_FUNCTIONS_H
#define __SPI_FUNCTIONS_H

#include <stm32f10x.h>
#include <misc.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_conf.h>
#include <stm32f10x_spi.h>

void spi2_master_init(void);

#endif
